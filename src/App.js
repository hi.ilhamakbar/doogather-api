import React, { Component } from 'react';

class App extends Component{
    constructor(props){
      super(props);
      this.state = {
        items: [],
        isLoaded: false,
      }
    }
  
    componentDidMount(){
      fetch('https://reqres.in/api/users')
        .then(res => res.json())
        .then(json => {
          this.setState({
            isLoaded: true,
            items: json,
          })
        });
    }
   
    render(){
      var { isLoaded, items } = this.state;      
  
      if(!isLoaded){
        return <div>Loading... Cannot fetch data</div>;
      }
      else{
        return(
          <div className="App">
            <table>
              <thead>
                <tr>
                  <th>id</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Photo</th>
                </tr>
              </thead>
              <tbody>
                {
                  items.data.map(item => (
                    <tr>
                      <td>{item.id}</td>
                      <td>{item.first_name}</td>
                      <td>{item.last_name}</td>
                      <td>{item.email}</td>
                      <div>
                        <img src={item.avatar} />
                      </div>                               
                    </tr>
                  ))
                }
              </tbody>
            </table>
          </div>
        );
      }    
    }
  }
  
  export default App;